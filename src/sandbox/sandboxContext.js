// eslint-disable-next-line no-unused-vars
import {ExpressDataContext, ExpressDataApplication} from "@themost/express";
import {DataConfigurationStrategy} from "@themost/data";
import {SandboxConfiguration} from "./sandboxConfiguration";
const applicationProperty = Symbol('application');

class SandboxContext extends ExpressDataContext {
    /**
     * @param {ExpressDataApplication} app - the parent data application of this context
     */
    constructor(app) {
        // noinspection JSCheckFunctionSignatures
        super(app.getConfiguration());
        // get application configuration
        const configuration = this.getConfiguration();
        /**
         * @type {SandboxConfiguration}
         */
        let sandboxConfiguration = configuration.getSourceAt('settings/universis/reports/sandbox');
        if (sandboxConfiguration == null) {
            sandboxConfiguration = new SandboxConfiguration();
        }
        // assign sandbox user
        this.user = {
            // set sandbox user or anonymous
            name: sandboxConfiguration.unattendedExecutionAccount || 'anonymous',
            // set authentication type (for future use)
            authenticationType: 'sandbox',
            // and scope to read (for future use)
            authenticationScope: 'read'
        }
        let _db;
        // override ExpressDataContext.getDb() and return sandbox data adapter
        this.getDb = function () {
            if (_db != null) {
                return _db;
            }
            // get adapter constructor
            const DataAdapter = configuration.getStrategy(DataConfigurationStrategy)
                .getAdapterType(sandboxConfiguration.adapter.invariantName)
                .createInstance;
            // create new data adapter
            _db = new DataAdapter(sandboxConfiguration.adapter.options);
            return _db;
        }
        // override ExpressDataContext.setDb()
        this.setDb = function () {
            // and trow error for readonly context
            throw new Error('Sandbox data context instance is readonly and cannot be set');
        }
        // set parent application
        this[applicationProperty] = app;
    }

    /**
     * Gets the parent data application
     * @returns {ExpressDataApplication}
     */
    getApplication() {
        return this[applicationProperty];
    }

}

export {SandboxContext};
