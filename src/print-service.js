import fetch from 'node-fetch';
import url from 'url';
import {ApplicationService, HttpError, Args, TraceUtils} from "@themost/common";
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication} from '@themost/express';
import {sandboxRouter} from './sandbox/router';
import {SandboxConfiguration} from "./sandbox/sandboxConfiguration";
const printSettingsProperty = Symbol('printSettings');
const URL = url.URL;
/**
 * @class
 * @property {string} server
 * @property {string} user
 * @property {string} password
 */
class ConfigurationPrintSettings {
    /**
     * @constructor
     */
    constructor() {
        //
    }
}

class PrintService extends ApplicationService {
    /**
     * @param {ExpressDataApplication|*} app
     */
    constructor(app) {
        super(app);
        /**
         * get service configuration from application configuration
         * @type {ConfigurationPrintSettings}
         */
        // set some default values
        // of course these values -like user and password- are useless in production environments
        // but follow a zero-configuration approach for a development environments
        this[printSettingsProperty] = Object.assign({
                server: 'http://localhost:8080/jasperserver/', // set default jasper server uri
                rootDirectory: 'reports/universis', // set default root directory
                user: 'jasperadmin', // default jasper server user
                password: 'jasperadmin', // default jasper server user password
            }, app.getConfiguration().getSourceAt('settings/universis/reports'),
            new ConfigurationPrintSettings());
        /**
         * @name PrintService#settings
         * @type ConfigurationPrintSettings
         */

        Object.defineProperty(this, 'settings', {
            get:()=> {
                return this[printSettingsProperty];
            }
        });

        // forcibly set sandbox configuration
        app.getConfiguration().setSourceAt('settings/universis/reports/sandbox', new SandboxConfiguration());
        // subscribe for application container
        if (app.container == null) {
            TraceUtils.info('Services PrintService application container cannot be found. Sandbox services will be disabled.');
        }
        else {
            TraceUtils.info('Services PrintService is waiting for application container to register sandbox services.');
            app.container.subscribe( container => {
                // get application container (typically an express application)
                if (container) {
                    // set sandbox service router
                    container.use('/sandbox/0', sandboxRouter(app));
                    TraceUtils.info('Services PrintService sandbox services has been successfully registered.');
                }
            });
        }
    }

    /**
     * @param {string} report
     * @param {string} format
     * @param {*=} queryParams
     * @param {*=} extraHeaders
     * @returns Promise<Buffer>
     * @example
     * const service = context.getApplication().getService(PrintService);
     * service.print('reports/Student Certificate','pdf', {
     *      "id": 54
     * }).then((result) => {
     *      fs.writeFile(path.resolve(process.env.HOME || process.env.USERPROFILE,'./Reports/StudentCertificate.pdf'), result, "binary", (err) => {
     *              return done(err);
     *      });
     * }
     */
    print(report, format, queryParams, extraHeaders) {
        return new Promise((resolve, reject) => {
            // validate report server URL
            Args.check(this.settings.server != null, new Error('Report server configuration is missing or is inaccessible.'));
            // validate report server user
            Args.check(this.settings.user != null, new Error('Report server configuration about service authentication is missing or cannot be found.'));
            /**
             * @type {string}
             * @example http://reports.example.com/jasperserver/rest_v2/reports/reports/Student Certificate.pdf
             * where server: http://reports.example.com/jasperserver/,
             * report: reports/Student Certificate and format:pdf
             */
            // format report path with extension e.g. reports/StudentCertificate.pdf
            const finalReport = report.concat('.', format.replace(/^\./, ''));
            // resolve report path relative to api root rest_v2/reports/
            const relativeURL = url.resolve('rest_v2/reports/', finalReport.replace(/^\//, ''));
            const reportURL = new URL(relativeURL, this.settings.server);
            // get basic authorization header value
            const basicAuthorization = new Buffer(`${this.settings.user}:${this.settings.password}`).toString('base64');
            // prepare headers
            const headers = Object.assign({
                //
            }, extraHeaders, {
                'Accept': 'application/json', // for error handling
                'Authorization': `Basic ${basicAuthorization}` // jasper server authorization
            });
            for (let [key, value] of Object.entries(queryParams)) {
                reportURL.searchParams.set(key, value);
            }
            // fetch report
            fetch(reportURL, {
                headers: headers
            })
            .then(async res => {
                if (res.ok === false) {
                    if (res.headers.get('content-type') === 'application/errorDescriptor+json') {
                        // get response error
                        const error = await res.json();
                        // and throw error
                        throw Object.assign(new HttpError(res.status), error);
                    }
                    throw new HttpError(res.status);
                }
                // otherwise return buffer
                return res.buffer();
            })
            .then(buffer => {
                // send buffer as method result
                return resolve(buffer);
            })
            .catch ( err => {
                return reject(err);
            });
        });
    }

    /**
     * Reads a report unit and returns report definition
     * @param {any} location
     * @returns {Promise<any>}
     */
    async readReport(location) {
        // get basic authorization header value
        const basicAuthorization = new Buffer(`${this.settings.user}:${this.settings.password}`).toString('base64');
        // set headers
        const headers = {
            'Accept': 'application/json', // for error handling
            'Authorization': `Basic ${basicAuthorization}`
        };
        const resourceURL = new URL('rest_v2/resources'.concat(location), this.settings.server);
        // fetch data
        const report = await fetch(resourceURL, {
            headers: headers
        })
            .then(async res => {
                if (res.ok === false) {
                    if (res.headers.get('content-type') === 'application/errorDescriptor+json') {
                        // get response error
                        const error = await res.json();
                        // and throw error
                        throw Object.assign(new HttpError(res.status), error);
                    }
                    throw new HttpError(res.status);
                }
                // otherwise return buffer
                return res.json();
            }).then( res => {
                return res;
            });
        if (Array.isArray(report.inputControls))
        {
            // fetch report parameters

            const reportParams = await this.readReportParameters(location);
            if (reportParams!=null && reportParams.inputControl != null)
            {
                const inputControlQueries = reportParams.inputControl.map(inputControl => {
                    return fetch(new URL('rest_v2/resources'.concat(inputControl.uri.replace('repo:', '')), 
                        this.settings.server
                    ), {
                        headers
                    })
                        .then(async response => {
                            const parsed = await response.json();
                            const dataTypeUrl = parsed.dataType.dataTypeReference.uri;
                            return fetch(new URL('rest_v2/resources'.concat(dataTypeUrl), this.settings.server), {
                                headers
                            }).then(async dataType => await dataType.json());
                        })
                });
                const inputControlData = await Promise.all(inputControlQueries);
                reportParams.inputControl.forEach((inputControl, i) => {
                    inputControl.dataType = {
                        ...inputControl.dataType,
                        ...inputControlData[i]
                    }
                });
                Object.assign(report, {inputControls:reportParams.inputControl});
            }

        }
        return report;
    }
    /**
     * Reads a report unit and returns report definition
     * @param {any} location
     * @returns {Promise<any>}
     */
    readReportParameters(location) {
        // get basic authorization header value
        const basicAuthorization = new Buffer(`${this.settings.user}:${this.settings.password}`).toString('base64');
        // set headers
        const headers = {
            'Accept': 'application/json', // for error handling
            'Authorization': `Basic ${basicAuthorization}`
        };
        const resourceURL = new URL('rest_v2/reports'.concat(location).concat('/inputControls'), this.settings.server);
        // fetch data
        return fetch(resourceURL, {
            headers: headers
        })
            .then(async res => {
                if (res.ok === false) {
                    if (res.headers.get('content-type') === 'application/errorDescriptor+json') {
                        // get response error
                        const error = await res.json();
                        // and throw error
                        throw Object.assign(new HttpError(res.status), error);
                    }
                    throw new HttpError(res.status);
                }
                // otherwise return buffer
                return res.json();
            }).then( res => {
                return res;
            });
    }
    /**
     * Returns an array of items which represents the available report units
     * @param {string} location
     * @returns {Promise<Array<ServerResource>>}
     */
    async readReports(location) {
        // get basic authorization header value
        const basicAuthorization = new Buffer(`${this.settings.user}:${this.settings.password}`).toString('base64');
        // set headers
        const headers = {
            'Accept': 'application/json', // for error handling
            'Authorization': `Basic ${basicAuthorization}`
        };
        const limit = this.settings.limit || 0;
        const resourceURL = new URL('rest_v2/resources', this.settings.server);
        // set query
        resourceURL.searchParams.set('folderUri', url.resolve('/', location));
        resourceURL.searchParams.set('type', 'reportUnit');
        resourceURL.searchParams.set('recursive', 'true');
        resourceURL.searchParams.set('limit', limit);
        // fetch data
        return fetch(resourceURL, {
            headers: headers
        })
        .then(async res => {
            if (res.ok === false) {
                if (res.headers.get('content-type') === 'application/errorDescriptor+json') {
                    // get response error
                    const error = await res.json();
                    // and throw error
                    throw Object.assign(new HttpError(res.status), error);
                }
                throw new HttpError(res.status);
            }
            // otherwise return buffer
            return res.json();
        }).then( res => {
            if (Array.isArray(res.resourceLookup)) {
                return res.resourceLookup;
            }
            // return empty array
            return [];
        });
    }

}

export {
    PrintService
};
